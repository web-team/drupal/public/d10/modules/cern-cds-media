<?php

namespace Drupal\cern_cds_media\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Defines the "cerncdsmedia" plugin.
 *
 * @CKEditorPlugin(
 *   id = "cerncdsmedia",
 *   label = @Translation("CernCdsMedia"),
 *   module = "cern_cds_media"
 * )
 */
class CernCdsMedia extends CKEditorPluginBase
{

  /**
   * {@inheritdoc}
   */
    public function getFile(): string
    {
        return CKEditorPluginBase::getModulePath('cern_cds_media') . '/plugins/cerncdsmedia/plugin.js';
    }

  /**
   * {@inheritdoc}
   */
    public function getDependencies(Editor $editor): array
    {
        return array();
    }

  /**
   * {@inheritdoc}
   */
    public function getLibraries(Editor $editor): array
    {
        return array();
    }

  /**
   * {@inheritdoc}
   */
    public function getConfig(Editor $editor): array
    {
        return array();
    }

  /**
   * {@inheritdoc}
   */
    #[ArrayShape(['CernCdsMedia' => "array"])] public function getButtons(): array
    {

        $path = CKEditorPluginBase::getModulePath('cern_cds_media') . '/';
        return [
        'CernCdsMedia' => [
        'label' => $this->t('CERN CDS Media'),
        'image' => $path . 'plugins/cerncdsmedia/images/cds.png',
        ],
        ];
    }
}
