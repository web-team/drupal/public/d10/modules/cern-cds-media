<?php
namespace Drupal\cern_cds_media\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\cern_cds_media\Plugin\Field\FieldType\CernCdsMedia;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Plugin implementation of the 'CernCdsMediaDefaultWidget' widget.
 *
 * @FieldWidget(
 *   id = "CernCdsMediaDefaultWidget",
 *   label = @Translation("CERN CDS Media"),
 *   field_types = {
 *     "CernCdsMedia"
 *   }
 * )
 */
class CernCdsMediaDefaultWidget extends WidgetBase
{
  /**
   * Define the form for the field type.
   *
   * Inside this method we can define the form used to edit the field type.
   *
   * Here there is a list of allowed element types: https://goo.gl/XVd4tA
   */
    public function formElement(
        FieldItemListInterface $items,
        $delta,
        Array $element,
        Array &$form,
        FormStateInterface $formState
    ): array {

        foreach (array_merge(CernCdsMedia::$metadata_text, CernCdsMedia::$metadata_string) as $property) {
            $element[$property] = [
            '#type' => 'textfield',
            '#title' => t(ucwords(str_replace('_', ' ', $property))),
            '#default_value' => $items[$delta]->{$property} ?? null,
            '#empty_value' => '',
            '#maxlength' => 350,
            ];
        }

        foreach (CernCdsMedia::$metadata_boolean as $property) {
            $element[$property] = [
            '#type' => 'checkbox',
            '#title' => t(ucwords(str_replace('_', ' ', $property))),
            '#return_value' => 1,
            '#default_value' => $items[$delta]->{$property} ?? null,
            '#attributes' => ['class' => ['video-field']]
            ];
        }

        foreach (CernCdsMedia::$metadata_integer as $property) {
            $element[$property] = [
            '#type' => 'number',
            '#title' => t(ucwords(str_replace('_', ' ', $property))),
            '#default_value' => $items[$delta]->{$property} ?? null,
            '#size' => 6,
            '#attributes' => ['class' => ['video-field']]
            ];
        }


        $element['record_id']['#attributes']['readonly'] = 'readonly';
        $element['type']['#attributes']['readonly'] = 'readonly';
        $element['size']['#type'] = 'hidden';
        $element['size']['#default_value'] = $items[$delta]->size ?? 'small';
        $element['download_url']['#attributes'] = ['class' => ['video-field']];
        $element['download_sizes']['#attributes'] = ['class' => ['video-field']];

        $fieldset['cern_cds_fieldset'] = [
        '#type' => 'details',
        '#title' => t('Metadata'),
        '#description' => t('CDS Resource metadata'),
        '#open' => false,
        ];

        foreach ($element as $property => $config) {
            $fieldset['cern_cds_fieldset'][$property] = $config;
        }
        $fieldset['cds_id'] = $fieldset['cern_cds_fieldset']['cds_id'];
        unset($fieldset['cern_cds_fieldset']['cds_id']);

        return $fieldset;
    }

  /**
   * Validate the CDS ID text field.
   */
    public static function validate($element, FormStateInterface $form_state)
    {
        $value = $element['#value'];
        if (empty($value)) {
            $form_state->setValueForElement($element, '');
        } else {
            $parents = $element['#parents'];
            array_pop($parents);
            $cds_values = NestedArray::getValue($form_state->getValues(), $parents);
            $record_id = $cds_values['cern_cds_fieldset']['record_id'];
            if (empty($record_id)) {
                $form_state->setError($element, t('Please click on "Get resource" button to retrieve the CDS information for ID: %id', ['%id' => $value]));
            }
        }
    }
} // class
