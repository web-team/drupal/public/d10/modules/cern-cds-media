# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.4] - 20/08/2024
- replace `cds.cern.ch/videos/<cds_id>` with `videos.cern.ch/video/<cds_id>`

## [3.0.3] - 26/08/2023
- drupal_get_path() and drupal_get_filename() have been deprecated in favor of extension listing services https://www.drupal.org/node/2940438 replaced all occurrences

## [3.0.2] - 31-10-2022
- Update usage of `languageManager()->getCurrentLanguage()` and import.
- Re-add the `composer.json` to maintain the version per infrastructure requirements.

## [3.0.1] - 31-10-2022
- Address missing `$langcode` in `viewElements`-function.
- Remove `composer.json` and `composer.lock` files.

## [3.0.0] - 09-09-2022

- Refactored per phpstan, phpmd, and php_codesniffer scans in preparation for Drupal 10 and PHP 8.1.

## [2.1.5] - 25-11-2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer.

## [2.1.4] - 08-02-2021

- Add core: 8.x to fix enabling issue

## [2.1.3] - 13-01-2021

- Update module to be D9-ready

## [2.1.2] - 04-12-2020

- Add composer.json file

## [2.1.1] - 24-04-2019

- Increased number of caption characters to 350

## [2.1.0] - 10-12-2018

- Re-structured files
